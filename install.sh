#!/usr/bin/env bash

PLATFORM=$(uname -s | tr A-Z a-z)
ARCH=$([[ $(uname -m) == arm64 ]] && echo arm64 || echo amd64)
KUBECTL_VERSION=$(curl -L -s https://dl.k8s.io/release/stable.txt)
HELM_VERSION=3.9.2
GOPATH=$(go env GOPATH)
TF_VERSION=1.2.6
PACKER_VERSION=1.8.3-dev

case $1 in
	"terraform")
		git clone github.com/hashicorp/terraform $(go env GOPATH)/src/terraform
		cd $(go env GOPATH)/src/terraform
		go install
		;;
	"packer")
		curl -LO https://github.com/hashicorp/packer/releases/download/nightly/packer_${PACKER_VERSION}_${PLATFORM}_${ARCH}.zip
		unzip packer_${PACKER_VERSION}_${PLATFORM}_${ARCH}.zip
		mv packer ${GOPATH}/bin
		rm packer_${PACKER_VERSION}_${PLATFORM}_${ARCH}.zip
		;;
	"helm")
		mkdir /tmp/helm && curl -SL https://get.helm.sh/helm-v${HELM_VERSION}-${PLATFORM}-${ARCH}.tar.gz | tar xz -C /tmp/helm --strip-components=1
		mv /tmp/helm/helm ${GOPATH}/bin
		rm -rf /tmp/helm
		;;
	"kubectl")
		curl -LO "https://dl.k8s.io/release/${KUBECTL_VERSION}/bin/${PLATFORM}/${ARCH}/kubectl"
		curl -LO "https://dl.k8s.io/${KUBECTL_VERSION}/bin/${PLATFORM}/${ARCH}/kubectl.sha256"
		SUM=$(cat ./kubectl.sha256); echo "$$SUM kubectl" | sha256sum --check
		rm kubectl.sha256
		chmod +x kubectl
		mv kubectl ${GOPATH}/bin
		;;
	"skaffold")
		curl -Lo skaffold https://storage.googleapis.com/skaffold/releases/latest/skaffold-${PLATFORM}-${ARCH}
		sudo install skaffold /usr/local/bin/
		rm skaffold
		;;
esac
