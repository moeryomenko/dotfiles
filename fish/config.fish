if status is-interactive
    # Commands to run in interactive sessions can go here
end

alias g=git
alias gco='g co'
alias glog='g glog'
alias ga='g a'
alias grb='g rb'
alias grbc='g rbc'
alias gcm='g cm'
alias gcmm='g cmm'
alias grc='g rc'
alias gcp='g cp'
alias gpo='g po'
alias gpfo='g pfo'
alias ur='ls | xargs -P10 -I{} git -C {} pull'
alias cor='ls | xargs -P10 -I{} git -C {} co main'
alias ll='exa -l -h --git --classify'
alias la='ll -a'
alias c=clear
alias bathelp='bat --plain --language=help'
alias nv=nvim
